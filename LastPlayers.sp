/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   LastPlayers.sp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: DragonSoul <dragonsoul@ovh.fr>             +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/06/16 01:07:12 by DragonSoul        #+#    #+#             */
/*   Updated: 2015/06/16 01:43:12 by DragonSoul       ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sourcemod>

new	String:g_players[5][2][64] = {{"", ""}, {"", ""}, {"", ""}, {"", ""}, {"", ""}};

public Plugin:myinfo =
{
	name = "LastPlayers",
	description = "Displays the 5 last disconnected players with their steamID",
	author = "DragonSoul",
	version = "0.1",
	url = "http://www.cscargo.net"
};

/*
** cr�ation de la commande
*/
public OnPluginStart()
{
	// changer tag selon besoin
	RegAdminCmd("sm_last", LastPlayers, ADMFLAG_KICK, "Displays last disconnected players");
}

public OnClientDisconnect(client)
{
	new String:name[64];
	new String:id[64];

	GetClientName(client, name, sizeof(name));
	GetClientAuthId(client, AuthId_Steam2, id, sizeof(id));
	AddPlayer(name, id);
}

/*
** d�cale les joueurs - mode bourrin !
*/
AddPlayer(const String:name[], const String:id[])
{
	strcopy(g_players[0][0], 64, g_players[1][0]);
	strcopy(g_players[0][1], 64, g_players[1][1]);
	strcopy(g_players[1][0], 64, g_players[2][0]);
	strcopy(g_players[1][1], 64, g_players[2][1]);
	strcopy(g_players[2][0], 64, g_players[3][0]);
	strcopy(g_players[2][1], 64, g_players[3][1]);
	strcopy(g_players[3][0], 64, g_players[4][0]);
	strcopy(g_players[3][1], 64, g_players[4][1]);
	strcopy(g_players[4][0], 64, name);
	strcopy(g_players[4][1], 64, id);
}

/*
** Affiche les joueurs - mode bourrin !
*/
public Action:LastPlayers(client, args)
{
	PrintToConsole(client, "List of 5 last players:");
	PrintToConsole(client, "1/ %s \t %s", g_players[0][0], g_players[0][1]);
	PrintToConsole(client, "2/ %s \t %s", g_players[1][0], g_players[1][1]);
	PrintToConsole(client, "3/ %s \t %s", g_players[2][0], g_players[2][1]);
	PrintToConsole(client, "4/ %s \t %s", g_players[3][0], g_players[3][1]);
	PrintToConsole(client, "5/ %s \t %s", g_players[4][0], g_players[4][1]);
	return	Plugin_Handled;
}



